### General Structure

PHP Factory Pattern && Test Driven Development

```php 
	class SpaceVehicle 
	{
		/**
		 * @var string $valid_commands
		 */
		protected $valid_commands;

		abstract function setPosition(int $x, int $y, string $cardinal_point);

		function setCommands(string $commands)
		{
			$this->valid_commands = $commands;
			
			return $this->valid_commands;
		}

		public function getValidCommands(): string
		{
			return $this->valid_commands;
		}
	}

	class Rover extends SpaceVehicle 
	{

	}

	class RoverFactory 
	{
		static public function Rover()
		{
			$rover = new Rover();
			$rover->setCommands('LRM');
			return $rover;
		}
	}

	class Driver 
	{
		const CARDINAL_POINTS = ['N', 'S', 'E', 'W'];

		protected $plateau;

		public function __construct(Plateau $plateau)
		{
			$this->plateau = $plateau;
		}

		public function drive(SpaceVehicle $vehicle, string $path)
		{

		}


	}
```

```php
	
	$plateau = new Plateau(5, 5);

	$driver = new Driver();

	$path = 'LMLMLMLMM';

	$driver
		->setPlateau($plateau)
		->setVehicle(RoverFactory::simpleRover())
		->drive($path);
```
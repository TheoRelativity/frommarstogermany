<?php

namespace Tests\MarsBundle\Entity;

use PHPUnit\Framework\TestCase;

use MarsBundle\Entity\SpaceVehicle;

use MarsBundle\Exception\InvalidCardinalPoint;
use MarsBundle\Exception\InvalidCoordinates;


class SpaceVehicleTest extends TestCase 
{
	public function testSetPosition()
	{
		$SpaceVehicle = new SpaceVehicle();

		$this->assertInstanceOf(SpaceVehicle::class, $SpaceVehicle->SetPosition(1, 1, 'N'));
	}

	/**
	 * @dataProvider InvalidCardinalPoints
	 */
	public function testItDoesNotPermitInvalidCardinalPoints(int $x, int $y, string $cardinal_point)
	{
		$SpaceVehicle = new SpaceVehicle();

		$this->expectException(InvalidCardinalPoint::class);

		$SpaceVehicle->setPosition($x, $y, $cardinal_point);
	}

	public function InvalidCardinalPoints()
	{
		return array(
			'unknown cardinal point' => [1, 1, 'C'],
			'invalid lower case'     => [1, 1, 'n']
		);
	}

	/**
	 * @dataProvider negativeCoordinates
	 */
	public function testItDoesNotPermitNegativeCoordinates(int $x, int $y, string $cardinal_point)
	{
		$SpaceVehicle = new SpaceVehicle();

		$this->expectException(InvalidCoordinates::class);

		$SpaceVehicle->setPosition($x, $y, $cardinal_point);
	}

	public function negativeCoordinates()
	{
		return array(
			[-1,  1,  'N'],
			[1,  -1,  'N'],
			[-1, -1,  'N']
		);
	}

	public function testSetCommands()
	{
		$SpaceVehicle = new SpaceVehicle();

		$this->assertInstanceOf(SpaceVehicle::class, $SpaceVehicle->setCommands('LRM'));
	}

	public function testGetValidCommands()
	{
		$SpaceVehicle = new SpaceVehicle();

		$commands = $SpaceVehicle
			->setCommands('LRM')
			->getValidCommands();

		$this->assertSame('LRM', $commands);
	}

}
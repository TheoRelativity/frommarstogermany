<?php

namespace Tests\MarsBundle\Entity;

use PHPUnit\Framework\TestCase;

use MarsBundle\Entity\Plateau;
use MarsBundle\Exception\InvalidPlateau;

class PlateauTest extends TestCase
{
	public function testForValidPlateau()
	{
		$plateau = new Plateau(1, 1);
		$this->assertSame([1, 1], $plateau->getMatrix());
	}

	/**
	 * @dataProvider getNullPlateaus
	 */
	public function testItDoesNotPermitNullPlateau(int $x, int $y)
	{
		$this->expectException(InvalidPlateau::class);

		$plateau = new Plateau($x, $y);
	}

	public function getNullPlateaus()
	{
		return array(
		'invalid x'		=>	[0,1],
		'invalid y'		=>	[1,0],
		'null x y'		=>	[0,0],
		'negative x y'	=>	[-1, -1],
		'negative x'	=>	[-1, 1],
		'negative y'	=>	[1, -1]
		);
	}

	/**
	 * @dataProvider isInData
	 */
	public function testIsIn(int $x, int $y, bool $isIn)
	{
		$plateau = new Plateau(10, 10);

		$this->assertSame($isIn, $plateau->isIn($x, $y));
	} 

	public function isInData()
	{
		return array(
			'is in'		=> [5, 5, true],
			'is not x'	=> [11, 5, false],
			'is not y'	=> [5, 11, false],
			'on edges'	=> [10, 10, true],
			'on x edge'	=> [10, 5, true],
			'on y edge' => [5, 10, true],
			'on 0 edge'	=> [0, 0, true]
		);
	}
}
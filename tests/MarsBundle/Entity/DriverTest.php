<?php

namespace Tests\MarsBundle\Entity;

use PHPUnit\Framework\TestCase;

use MarsBundle\Entity\Driver;
use MarsBundle\Entity\Plateau;
use MarsBundle\Factory\RoverFactory;

class DriverTest extends TestCase 
{
	/* public function testValidPath()
	{
		$driver = new Driver();

		$this->assertInstanceOF(Driver::class, $driver->setPath('LMLMLMLMM'));
	} */

	public function testSetPlateau()
	{
		$driver = new Driver();

		$this->assertInstanceOF(Driver::class, $driver->setPlateau(new Plateau(10, 10)));

	}

	public function testSetVehicle()
	{
		$driver = new Driver();

		$this->assertInstanceOF(Driver::class, $driver->setVehicle(RoverFactory::simpleRover()));
	}
}
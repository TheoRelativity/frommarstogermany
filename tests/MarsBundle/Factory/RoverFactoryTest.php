<?php

namespace Tests\MarsBundle\Factory;

use PHPUnit\Framework\TestCase;

use MarsBundle\Factory\RoverFactory;
use MarsBundle\Entity\Rover;

class RoverFactoryTest extends TestCase 
{

	public function testSimpleRover()
	{
		$rover = new RoverFactory();

		$this->assertInstanceOf(Rover::class, $rover->simpleRover());
	}

	public function testIsValidPath()
	{
		$factory = new RoverFactory();

		$rover = $factory->simpleRover();

		$this->assertSame(true, $rover->isValidPath('LMRLMRLMR'));
	}
}
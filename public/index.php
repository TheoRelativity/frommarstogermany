<?php

require_once __DIR__.'/../vendor/autoload.php';

use MarsBundle\Factory\RoverFactory;
use MarsBundle\Entity\Plateau;
use MarsBundle\Entity\Driver;

$plateau = new Plateau(5, 5);

$driver = new Driver();

$path = 'LMLMLMLMM';

$driver
	->setPlateau($plateau)
	->setVehicle(RoverFactory::simpleRover())
	->drive($path);



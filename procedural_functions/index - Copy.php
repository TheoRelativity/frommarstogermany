<?php

require 'RoversController.php'; 

$roversDriver = new RoverController();


$rover = [1, 2, 'N'];
$path = 'LMLMLMLMM';

$rover_or = 'N';


if ( $roversDriver->isValidPath($path) )
{
	$steps = $roversDriver->getSteps($path);
	
	echo "PATH: $path <br/><br/>";
	
	foreach($steps as $step)
	{
	
		$Ms = $roversDriver->summation($step);
		
		$rotations_str = substr($step, 0, strlen($step) - $Ms);
		
		echo 'Step: ' . $step . '</br>';
		echo 'Rotations: ' . $rotations_str. '<br/>';
		echo 'Movements: ' . $Ms . '<br/>';
		
		$rotations = str_split($rotations_str);
		
		foreach($rotations as $or)
		{
			$new_or = strval($roversDriver->{$rover[2]} + $roversDriver->{$or});
			
			$func = $roversDriver->getFuncFromDegrees($new_or);
			
			echo 'New Orientation: '. $func .'<br/>';
			
			$rover[2] = $func;
			
			$rover = $roversDriver->{$func}($rover, $Ms);
			
			echo 'New Rover Coordinates: '. implode($rover) .'<br/>';
		}
		
		echo '<br/>';
		
	}
}
else echo 'Invalid Path';

// $roversDriver->getPaths();
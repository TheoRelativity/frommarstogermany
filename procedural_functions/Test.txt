Una squadra di rover robotici sarà sbarcata dalla NASA su un altopiano su Marte.

Questo pianoro, curiosamente rettangolare, deve essere navigato dai rover in modo 

che le loro telecamere di bordo possano ottenere una visione completa del terreno 

circostante da inviare sulla Terra.

La posizione di un rover è rappresentata da una combinazione di coordinate xey e 

una lettera che rappresenta uno dei quattro punti cardinali della bussola. 

L'altopiano è diviso in una griglia per semplificare la navigazione. 

Una posizione di esempio potrebbe essere 0, 0, N, il che significa che il rover 

si trova nell'angolo in basso a sinistra e rivolto verso Nord.

Al fine di controllare un rover, la NASA invia una semplice stringa di lettere. 

Le lettere possibili sono L, R e M. L e R fa ruotare il rover di 90 ° 

a sinistra o a destra, rispettivamente, senza spostarsi dal punto corrente.

 Ingresso:
La prima riga di input è le coordinate in alto a destra del plateau, 

le coordinate in basso a sinistra sono assunte da 0,0.

Il resto dell'input sono informazioni relative ai rover che sono stati distribuiti.

Ogni rover ha due linee di input. La prima riga dà la posizione del rover e 

la seconda riga è una serie di istruzioni che dicono al rover come esplorare l'altopiano.

La posizione è composta da due numeri interi e una lettera separati da spazi, 

corrispondenti alle coordinate xey e all'orientamento del rover.

Ogni rover sarà terminato in sequenza, il che significa che il secondo 

rover non inizierà a muoversi fino a quando il primo avrà finito di muoversi.

M significa andare avanti di un punto della griglia e mantenere la stessa direzione. 

Supponiamo che il quadrato direttamente da Nord (x, y) sia (x, y + 1).


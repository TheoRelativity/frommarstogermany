<?php
/**
 *
 * Mars Challenge from Metro Markets
 *
 */
class RoverController
{
	const
	
	DEBUG_ON = true,
	DEBUG_VERBOSE = false;
	
	protected
	
	$N = 360,
	$E = 90,
	$S = 180,
	$W = 270,
	
	$L = -90,
	$R = 90,
			
	$cardinal_points = ['N', 'S', 'E', 'W'],
			
	$cardinal_points_degrees = [ 0 => 'N', 360 => 'N', 450 => 'E', 90 => 'E', 180 => 'S', 270 => 'W' ];
	
	private
	
	/**
	 * The rover position
	 * valid example [1,2,'N']
	 */
	$position,
	
	/**
	 * The plateaus's matrix
	 * valid example [5,5]
	 */
	$plateau,
	
	/**
	 * The rover path to follow
	 * valid example 'LMRMMRLLM'
	 */
	$path;
	
	/**
	 * @var array $position
	 *
	 * @retun $this or false
	 *
	 * definition: (int, int, string)
	 * 
	 * valid example: (10, 2, 'N')
	 *
	 */
	public function setPosition($position)
	{
		if ( $this->isPositionValid($position) )
		{
			$this->position = $position;
			return $this;
		}
		
		$this->position = null;
		
		if ($this::DEBUG_ON) throw new Exception('Invalid Position');
		
		return false;
		
	}
	
	/**
	 * @var array $position
	 *
	 * definition: (int > 0, int > 0, string {'N', 'S', 'E', 'W'})
	 * 
	 * valid example: (10, 2, 'N')
	 */
	protected function isPositionValid($position)
	{
		 if(	!is_array($position)
				||  !isset($position[0])
				||  !is_int($position[0])
				||  !isset($position[1])
				||  !is_int($position[1])
				||  !isset($position[2])
				||	$position[0] <= 0
				||	$position[1] <= 0
				||  !in_array($position[2], $this->cardinal_points)
			)
		{
			return false;
		}
		
		return true;
	}

	/**
	 * @var string $path
	 *
	 * 
	 * valid example: 'LMLMLMMRM'
	 */
	public function setPath($path)
	{
		
		if ( $this->isPathValid($path) )
		{
			$this->path = $path;
			return $this;
		}
		
		$this->path = null;
		
		if ($this::DEBUG_ON) throw new Exception('Invalid Path');
		
		return false;
	}
	
	/**
	 *
	 * @var array plateau
	 *
	 */
	public function setPlateau($plateau)
	{
		if ( $this->isPlateauValid($plateau) )
		{
			$this->plateau = $plateau;
			return $this;
		}
		
		$this->plateau = null;
		
		if ($this::DEBUG_ON) throw new Exception('Invalid Plateau');
		
		return false;
	}
	
	protected function isPlateauValid($plateau)
	{
		
		if(		!is_array($plateau)
			||  !isset($plateau[0])
			||  !is_int($plateau[0])
			||  !isset($plateau[1])
			||  !is_int($plateau[1])
			||	$plateau[0] < 0
			||	$plateau[1] < 0
			||  ($plateau[0] + $plateau[1]) <= 0
		  )
		{
			if ($this::DEBUG_ON) throw new Exception('Invalid Plateau');
			
			return false;
		}
		
		return true;
		
	}
	
	/**
	 *
	 * This is the method that makes the rover rotate and move
	 */
	public function start()
	{
		
		if (	is_null($this->position)
			||	is_null($this->path)
			||  is_null($this->plateau)
			)
		{
			if ($this::DEBUG_ON) throw new Exception('Invalid Data. Did you forget to set the Path or the rover Position or the Plateau?');
			
			return false;
		}
			
		$steps = $this->getSteps($this->path);
		
		if ($this::DEBUG_VERBOSE )
		{
			echo
				'PLATEAU: '.implode(",", $this->plateau).'<br/>'.
				"PATH: {$this->path} <br/>".
				'POSITION: ' .implode(",", $this->position). '<br/><br/>';
		}
		
		foreach($steps as $step)
		{
	
			$Ms = $this->extMovements($step);
		
			if ($this::DEBUG_VERBOSE )
			{
				echo
					"Step:	$step  </br>".
					"Movements: $Ms<br/>";
			}
		
			$rotations = $this->extRotations($step, $Ms);
			
			$new_orientation = $this->rotate($rotations);
			
			$this->move($new_orientation, $Ms);
			
		
		}
		
		return $this->position;
	}
	
	/**
	 *
	 * Rotates the rover and returns the new orientation
	 */
	protected function rotate($rotations)
	{
		
		foreach($rotations as $or)
		{
			$degrees = strval($this->{$this->getOrientation()} + $this->{$or});
			
			$new_orientation = $this->getFuncFromDegrees($degrees);
			
			$this->position[2] = $new_orientation;
			
			if ($this::DEBUG_VERBOSE )
			{
				echo 'New Orientation: '. $this->getOrientation() .'<br/>';
			}
		}
		
		return $new_orientation ?? $this->getOrientation();
		
	}
	
	/**
	 * Moves the rover
	 *
	 * @var string	$orientation
	 * @var int	$movements
	 */
	protected function move($orientation, $movements)
	{
		if ( !is_null($this->position) && $movements > 0 )
		{
			$new_position = $this->{$orientation}($this->position, $movements);
			
			if ( $this->isMovSafe($new_position) )
			{
				$this->position = $new_position;
				
				if ($this::DEBUG_VERBOSE )
				{
					echo 'New Rover Coordinates: '. implode($this->position) .'<br/>';
				}
				
			}
			else
			{
				if ($this::DEBUG_ON) throw new Exception('The rover cannot be moved outside the plan!<br/>Plateau:' . implode($this->plateau) .'<br/>Invalid position: '.implode($new_position));
				return false;
			}
			
		}
		
	}
	
	protected function isMovSafe($new_position)
	{
		if(		($new_position[0] >= 0 && $new_position[0] <= $this->plateau[0])
			&&	($new_position[1] >= 0 && $new_position[1] <= $this->plateau[1])
		)
		{
			return true;
		}
		
		return false;
	}
	
	/*
	 *
	 * Extract the Rotations'sequence from the step string
	 *
	 * @var string $step
	 * @var int    $Ms
	 * @return array
	 *
	 */
	protected function extRotations($step, $Ms)
	{
		$rotations_str = substr($step, 0, strlen($step) - $Ms);
		
		if($this::DEBUG_VERBOSE) echo "Rotations: \"$rotations_str\" <br/>";
		
		return $rotations_str !='' ? str_split($rotations_str) : [];
	}
	
	public function getFuncFromDegrees($degrees)
	{
		return array_key_exists($degrees, $this->cardinal_points_degrees) ? $this->cardinal_points_degrees[$degrees] : false;
	}
	
	/**
	 * 
	 * North: moves the y up
	 */
	function N(array $rover, $sum_of_Ms)
	{
		$rover[1] = $rover[1] + $sum_of_Ms;
		
		return $rover;
	}

	/**
	 * 
	 * South: moves the y down
	 *
	 */
	function S(array $rover, $sum_of_Ms)
	{
		$rover[1] = $rover[1] - $sum_of_Ms;
		
		return $rover;
	}

	/**
	 * 
	 * Eastern: move the x to the right
	 *
	 */
	function E(array $rover, $sum_of_Ms)
	{
		$rover[0] = $rover[0] + $sum_of_Ms;
		
		return $rover;
	}

	/**
	 * 
	 * Western: move the x to the left
	 *
	 */
	function W(array $rover, $sum_of_Ms)
	{
		$rover[0] = $rover[0] - $sum_of_Ms;
		
		return $rover;
	}
	
	/**
	 * Checks if the path is valid
	 */
	public function isPathValid(string $path)
	{
		if ($path == '') return false;
		
		$re = '/[^LRM]/m';
		
		$invalid = preg_match($re, $path, $matches);
		
		if ( $invalid === false ) return false;
		
		if ( $invalid === 1 ) 
		{
			//var_dump($matches);
			return false;
		}
		else
		{
			//echo 'valid path\r\n';
			return true;
		}
		
	}
		
	/**
	 *
	 * extract steps from the path
	 * 
	 */
	public function getSteps(string $path)
	{
		$re = '/([^M]+M+|[^M]+|M+)/mx';
		
		$total_steps = preg_match_all($re, $path, $found, PREG_SET_ORDER, 0);
		
		if ( $total_steps !== false && $total_steps > 0 )
		{
			$steps = [];
			
			foreach($found as $matches => $step)
			{
				$steps[] = $step[1];
			}
			
		}
		else
		{
			return false;
		}
		
		//var_dump($steps);
		
		return $steps;
	}
	
	/**
	 * counts the Ms in the step string 
	 */
	public function extMovements(string $step)
	{
		$re = '/M/mx';

		$Ms = preg_match_all($re, $step);
		
		return $Ms;
	}
	
	/**
	 *
	 * Returns the x position of the rover
	 *
	 */
	public function getX()
	{
		
		if ( !is_null($this->position) )
		{
			return $this->position[0];
		}
		
		if ($this::DEBUG_ON) throw new Exception('x not set. Did you forget to use setPosition?');
			
		return false;
		
	}
	
	/**
	 *
	 * Returns the y position of the rover
	 *
	 */
	public function getY()
	{
		if ( !is_null($this->position) )
		{
			return $this->position[1];
		}
		
		if ($this::DEBUG_ON) throw new Exception('y not set. Did you forget to use setPosition?');
			
		return false;
		
	}

	/**
	 *
	 * Return the orientation of the rover
	 *
	 */
	public function getOrientation()
	{
		if ( !is_null($this->position) )
		{
			return $this->position[2];
		}
		
		if ($this::DEBUG_ON) throw new Exception('Orientation not set. Did you forget to use setPosition?');
			
		return false;
		
	}
}
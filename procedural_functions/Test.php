<?php

require 'RoversController.php';

$totest = new RoversController();

/**
 *
 * Tests on getSteps() 
 *
 */

echo "Tests on getSteps() <br/>";

$test_steps =
	[
		'M'  =>	['steps' => 1],
		'R'  =>	['steps' => 1],
		'L'  =>	['steps' => 1],
		'MM' =>	['steps' => 1],
		'RM' =>	['steps' => 1],
		'LM' => ['steps' => 1],
		''   => ['steps' => false]
	];
	
foreach($test_steps as $path => $data)
{
	$steps = $totest->getSteps($path);
	
	if ( $steps === false )
	{
		if ( $data['steps'] === false )
		{
			echo "Test \"$path\" PASSED </br>";
		}
		else 
		{
			echo "Test \"$path\" <strong>FAILED</strong></br>";
		}
	}
	else if ( count($steps) !== $data['steps'] )
	{
		echo "Test \"$path\" <strong>FAILED</strong></br>";
	}
	else
	{
		echo "Test \"$path\" PASSED </br>";
	}
}

/**
 * Tests on pathIsValid()
 */

echo "Tests on isValidPath() <br/>";

$test_paths =
	[
		'M'   =>	['return' => true],
		'R'   =>	['return' => true],
		'L'   =>	['return' => true],
		'MM'  =>	['return' => true],
		'RM'  =>	['return' => true],
		'LM'  => 	['return' => true],
		'lll' => 	['return' => false],
		''    =>    ['return' => false]
	];
	
foreach($test_paths as $path => $data)
{
	$valid = $totest->isValidPath($path);
	
	if ( $valid === $data['return'] )
	{
		echo "Test $path PASSED<br/>";
	}
	else
	{
		echo "Test $path <strong>FAILED</strong><br/>";
	}
}

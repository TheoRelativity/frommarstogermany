<?php

namespace MarsBundle\Entity;

use MarsBundle\Entity\Plateau;

use MarsBundle\Exception\InvalidPath;

class Driver 
{
	/**
	 * @var string $path
	 */
	private $path;

	/**
	 * @var Plateau
	 */
	private $plateau;

	/**
	 * @var SpaceVehicle
	 */
	private $vehicle;


	public function drive(string $path)
	{
		if ( $this->vehicle->isValidPath($path) ) return false;
	}

	public function setPlateau(Plateau $plateau): Driver
	{
		$this->plateau = $plateau;

		return $this; 
	}

	public function setVehicle(spaceVehicle $vehicle): Driver
	{
		$this->vehicle = $vehicle;

		return $this;
	}

/* public function setPath(string $path)
	{

		$re = '/[^'.$this::VALID_PATH_CHARACTERS.']/';
		
		$invalid = preg_match($re, $path);
		
		if ( $invalid === false || $invalid === 1 ) 
		{
			throw new InvalidPath();
		}
		
		$this->path = $path;
		
		return $this;
		
	} */

}
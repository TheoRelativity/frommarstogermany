<?php

namespace MarsBundle\Entity;

use MarsBundle\Exception\InvalidCardinalPoint;
use MarsBundle\Exception\InvalidCoordinates;

class SpaceVehicle 
{

	/**
	 * @var array
	 */
	const CARDINAL_POINTS = ['N', 'S', 'E', 'W'];
	
	/**
	 * @var string $valid_commands
	 */
	protected $valid_commands;

	/**
	 * @var array
	 */
	protected $position;

	final public function setPosition(int $x, int $y, string $cardinal_point): SpaceVehicle
	{
		if (!in_array($cardinal_point, $this::CARDINAL_POINTS))
		{
            throw new InvalidCardinalPoint();
        }

        if ($x < 0 || $y < 0)
        {
        	throw new InvalidCoordinates();
        }

		$this->position = array($x, $y, $cardinal_point);

		return $this;
	}
	
	public function getPosition(): array
	{
		return $this->position;
	}

	function setCommands(string $commands): SpaceVehicle
	{
		$this->valid_commands = $commands;
			
		return $this;
	}

	public function getValidCommands(): string
	{
		return $this->valid_commands;
	}

	public function isValidPath(string $path): bool
	{
		$re = '/[^'.$this->valid_commands.']/';
		
		$invalid = preg_match($re, $path);
		
		return ( $invalid === false || $invalid === 1 ) ? false : true;
	}
}
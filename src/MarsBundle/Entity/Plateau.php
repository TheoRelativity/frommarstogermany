<?php

namespace MarsBundle\Entity;

use MarsBundle\Exception\InvalidPlateau;

class Plateau 
{
	/**
	 * @var array
	 */
	private $matrix;

	public function __construct(int $x, int $y)
	{
		if ($x <= 0 || $y <= 0)
		{
			throw new InvalidPlateau();
		}

		$this->matrix = array($x, $y);
	}

	public function getMatrix(): array
	{
		return $this->matrix;
	}

	public function isIn(int $x, int $y): bool
	{
		return ($x >= 0 && $x <= $this->matrix[0]) && ($y >= 0 && $y <= $this->matrix[1]);
	}
}
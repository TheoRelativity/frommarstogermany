<?php

namespace MarsBundle\Factory;

use MarsBundle\Entity\Plateau;
use MarsBundle\Entity\Rover;
use MarsBundle\Entity\Driver;

class RoverFactory 
{
	public function simpleRover(): Rover
	{	
		$rover = new Rover();

		$rover->setCommands('LRM');

		return $rover;
	}
}
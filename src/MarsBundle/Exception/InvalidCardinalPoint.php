<?php

namespace MarsBundle\Exception;

class InvalidCardinalPoint extends \Exception 
{
	protected $message = 'Please use a valid cardinal point. Choose among: N, S, E and W';
}
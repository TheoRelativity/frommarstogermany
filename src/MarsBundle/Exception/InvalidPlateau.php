<?php

namespace MarsBundle\Exception;

class InvalidPlateau extends \Exception
{
	protected $message = 'Invalid Plateau. Coordinates must be positive values greater than 0.';
}
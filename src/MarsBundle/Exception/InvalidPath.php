<?php

namespace MarsBundle\Exception;

class InvalidPlateau extends \Exception
{
	protected $message = 'Invalid Path.';
}
<?php

namespace MarsBundle\Exception;

class RoverOutsideThePlateau extends \Exception 
{
	protected $message = 'Invalid position. You are moving the rover outside the plateau.';
}
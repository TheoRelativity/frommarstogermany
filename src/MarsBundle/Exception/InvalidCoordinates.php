<?php

namespace MarsBundle\Exception;

class InvalidCoordinates extends \Exception 
{
	protected $message = 'Please use a valid coordinates. Coordinates must be positive integers.';
}